# Tensorflow и утилиты к нему
import numpy as np
import os
import PIL
import PIL.Image
import tensorflow as tf
import tensorflow_datasets as tfds
from tensorflow.keras import layers, regularizers # regulizers - для управления переобучением

# Утилита работы с путями
import pathlib

# Подготовка
print('Версия Tensorflow:')
print(tf.__version__)

# Коллекция изображений цветов
print('Загружаем и распаковываем изображения')
dataset_url = "https://storage.googleapis.com/download.tensorflow.org/example_images/flower_photos.tgz"

# Скачать и распаковать архив изображений
data_dir = tf.keras.utils.get_file(origin=dataset_url, fname='flower_photos', untar=True)

# Построить путь
data_dir = pathlib.Path(data_dir)

# Провеяем
print('Найдено изображений')
image_count = len(list(data_dir.glob('*/*.jpg')))
print(image_count)

# Настройка обработчика tf.keras.preprocessing
num_classes = 5 # Число классов в наборе данных
batch_size = 32
img_width = 128
img_height = 128

# Изображения для обучения (80% для обучения, 20% для проверки/теста)
print('Набор для обучения')
training_dataset = tf.keras.preprocessing.image_dataset_from_directory(data_dir,
    validation_split=0.2,
    subset='training',
    seed=123,
    image_size=(img_width, img_height),
    batch_size=batch_size)

# Изображения для валидации
print('Набор для проверки')
validation_dataset = tf.keras.preprocessing.image_dataset_from_directory(data_dir,
    validation_split=0.2,
    subset='validation',
    seed=123,
    image_size=(img_width, img_height),
    batch_size=batch_size)

# Классы из набора обучения
class_names = training_dataset.class_names
print('Классы:')
print(class_names)

# Создаем модель
# Sequential - последовательная модель
# Rescaling - нормализация цвета от 0-255 до 0-1
# 32 - количество объектов в классе
# 3 - количество каналов цвета, нужен для размера матрицы
#
# Conv2D - сверточный слой
# MaxPool2D - номрализация слоя, т.е. каждый выхов в 2 раза уменшает слой на выходе
# Flatten - конвертирует вектор в число (скаляр), переумножая значения
print('Сборка модели')
model = tf.keras.Sequential([
    tf.keras.layers.experimental.preprocessing.Rescaling(1./255),
    tf.keras.layers.Conv2D(32, 3, activation='relu'),
    tf.keras.layers.MaxPool2D(),
    tf.keras.layers.Conv2D(32, 3, activation='relu'),
    tf.keras.layers.MaxPool2D(),
    tf.keras.layers.Conv2D(32, 3, activation='relu'),
    tf.keras.layers.Flatten(),
    tf.keras.layers.Dense(256, activation='relu'),
    tf.keras.layers.Dense(256, activation='relu'),
    tf.keras.layers.Dense(256, activation='relu'),
    tf.keras.layers.Dense(256, activation='relu'),
    tf.keras.layers.Dense(256, activation='relu'),
    tf.keras.layers.Dense(num_classes)
])

# Оптимизировать модель для обучения
model.compile(
    optimizer='adam',
    loss=tf.losses.SparseCategoricalCrossentropy(from_logits=True),
    metrics=['accuracy'])

# Запуск тренировки модели на CPU (3 поколения)
print('Запуск обучения')
model.fit(
    training_dataset,
    validation_data=validation_dataset,
    epochs=3
)

#
